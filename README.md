# PHPCMF小程序DEMO
本demo提供微信小程序的初级接口实例，仅供学习参考使用，适用于微信小程序开发者、移动端原生APP开发者也可以参考接口的使用方法，开发者可按需来开发。

[http://help.phpcmf.net/416.html](http://help.phpcmf.net/416.html)

本demo包含的功能有：
任意地址数据读取接口、评论列表接口、发评论接口、点赞接口、收藏/取消接口、
登录接口、注册接口、微信小程序登录接口、微信小程序付款接口、会员中心任意列表页面数据读取接口
修改密码接口、头像上传接口、资料修改接口
文件上传接口、模块内容发布接口

通过自带的意思接口，无论是微信小程序还是原生态APP都会有很大的帮助。


![输入图片说明](https://git.oschina.net/uploads/images/2017/0916/022253_309b095a_5382.png "xcx2.png")
![输入图片说明](https://git.oschina.net/uploads/images/2017/0916/022307_9500ff48_5382.png "xcx3.png")
![输入图片说明](https://git.oschina.net/uploads/images/2017/0916/022317_79c5cc1a_5382.png "xcx4.png")
![输入图片说明](https://git.oschina.net/uploads/images/2017/0916/022327_1d40e0ca_5382.png "wcx5.png")
![输入图片说明](https://git.oschina.net/uploads/images/2017/0916/022338_17a4ef5a_5382.png "xcx6.png")